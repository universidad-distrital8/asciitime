class asciitime():
	def __init__(self):
		self.format = '12h'
		
	def seg7(self,n):
		s0 = [' __  ','     ',' __  ',' __  ',
	        '     ',' __  ',' __  ',' __  ',
	        ' __  ',' __  ']
	      
		s1 = ['|  | ','   | ',' __| ',' __| ',
	   	   '|__| ','|__  ','|__  ','   | ',
	   	   '|__| ','|__| ']
	      
		s2 = ['|__| ','   | ','|__  ',' __| ',
	      	'   | ',' __| ','|__| ','   | ',
	      	'|__| ',' __| ']
	      
		return [s0[n], s1[n], s2[n]]
	
	def print_time(self,hour, min):
		if self.format == '12h' and hour > 12:
			hour -= 12
		elif self.format != '24h':
			print('wrong time format (12h or 24h)')
			return
			
		hour_d = hour // 10
		hour_u = int(hour - hour_d*10)
		min_d  = min // 10
		min_u  = int(min - min_d*10)
		
		[t0a, t1a, t2a] = self.seg7(hour_d)
		[t0b, t1b, t2b] = self.seg7(hour_u)
		[t0c, t1c, t2c] = self.seg7(min_d)
		[t0d, t1d, t2d] = self.seg7(min_u)
			
		print('   '+t0a +t0b +'   '+t0c +t0d)
		print('   '+t1a +t1b +' * '+t1c +t1d)
		print('   '+t2a +t2b +' * '+t2c +t2d)
		
		
if __name__ == '__main__':
	from datetime import datetime
	at = asciitime()
	now = datetime.now()
	at.print_time(now.hour,now.minute)
	at.format = '24h'
	at.print_time(now.hour,now.minute)
	